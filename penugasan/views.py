from django.shortcuts import render, redirect
from django.db import connection
import datetime

def daftar_penugasan(request):
    cursor = connection.cursor()
    if request.method == 'POST':
        if request.POST.get('update-penugasan'):
            pass
        elif request.POST.get('delete-penugasan'):
            ktp = request.POST['delete-penugasan'].split("#")[0]
            id_stasiun = request.POST['delete-penugasan'].split("#")[1]
            cursor.execute("""DELETE FROM PENUGASAN
                              WHERE ktp = '{}'
                                    AND id_stasiun = '{}'
                           """.format(ktp, id_stasiun))
            
            cursor.execute("SELECT P.ktp, P.nama AS nama_petugas, PG.*, ST.nama AS nama_stasiun "
                           "FROM PENUGASAN AS PG, PETUGAS AS PT, PERSON AS P, STASIUN AS ST "
                           "WHERE PG.ktp = PT.ktp "
                           "AND PT.ktp = P.ktp "
                           "AND PG.id_stasiun = ST.id_stasiun")
            data = dictfetchall(cursor)
            return render(request, 'daftar_penugasan.html', {'data_penugasan':data})

    else:
        with connection.cursor() as cursor:
            cursor.execute("SELECT P.ktp, P.nama AS nama_petugas, PG.*, ST.nama AS nama_stasiun "
                           "FROM PENUGASAN AS PG, PETUGAS AS PT, PERSON AS P, STASIUN AS ST "
                           "WHERE PG.ktp = PT.ktp "
                           "AND PT.ktp = P.ktp "
                           "AND PG.id_stasiun = ST.id_stasiun")
            data_penugasan = dictfetchall(cursor)
        return render(request, 'daftar_penugasan.html', {'data_penugasan':data_penugasan})

def create_penugasan(request):
    if (request.method == 'POST'):
        petugas = request.POST['petugas']
        waktu_mulai = request.POST['waktu_mulai']
        waktu_selesai = request.POST['waktu_selesai']
        stasiun = request.POST['stasiun']

        with connection.cursor() as cursor:
            query = '''
                    INSERT INTO PENUGASAN VALUES
                    ('{}', '{}', '{}', '{}');
                    '''.format(petugas, waktu_mulai, stasiun, waktu_selesai)
            cursor.execute(query)
        return redirect('daftar_penugasan')
    
    else:
        with connection.cursor() as cursor:
            cursor.execute('SELECT * FROM STASIUN')
            stasiun_list = dictfetchall(cursor)

            cursor.execute('''SELECT PTG.*, P.*
                              FROM PETUGAS PTG, PERSON P 
                              WHERE PTG.ktp = P.ktp;
                           ''')
            petugas_list = dictfetchall(cursor)

            response = {'stasiun_list': stasiun_list,
                        'petugas_list': petugas_list,
                        }

        return render(request, 'create_penugasan.html', response)

def update_penugasan(request):
    return render(request, 'update_penugasan.html')

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
from django.urls import include, path
from .views import daftar_penugasan, create_penugasan, update_penugasan

urlpatterns = [
    path('', daftar_penugasan, name='daftar_penugasan'),
    path('create/', create_penugasan, name='create_penugasan'),
    path('update/', update_penugasan, name='update_penugasan'),
]

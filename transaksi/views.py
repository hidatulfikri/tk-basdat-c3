from django.shortcuts import render, redirect
from django.db import connection
import datetime


def daftar_transaksi(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM TRANSAKSI")
        data_transaksi = dictfetchall(cursor)
    return render(request, 'daftar_transaksi.html', {'data_transaksi':data_transaksi})

def create_topup(request):
    if (request.method == "POST"):
        nominal = request.POST['nominal']
        no_kartu_agt_static = 150  #nanti ini diganti dengan anggota yg lagi login
        datetime_object = datetime.datetime.now()

        # proses menyimpan ke sql (insert)
        with connection.cursor() as cursor:
            query = '''
                    INSERT INTO TRANSAKSI VALUES
                    ('{}', '{}', 'Top Up', {});
                    '''.format(no_kartu_agt_static, datetime_object, nominal)
            cursor.execute(query)

        return redirect('daftar_transaksi')
    else:
        return render(request, 'create_topup.html')

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
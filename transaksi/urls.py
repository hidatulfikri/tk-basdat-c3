from django.urls import include, path
from .views import create_topup, daftar_transaksi

urlpatterns = [
    path('', daftar_transaksi, name='daftar_transaksi'),
    path('topup/', create_topup, name='create_topup'),
]

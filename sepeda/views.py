from django.shortcuts import render, redirect
from django.db import connection
from django.http import HttpResponseRedirect

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def daftar_sepeda(request):
    cursor = connection.cursor()
    if request.method == 'POST':
        if request.POST.get('update-acara'):
            pass
        elif request.POST.get('delete-sepeda'):
            id_sepeda = request.POST['delete-sepeda']
            cursor.execute("DELETE FROM SEPEDA WHERE nomor = '{}'".format(id_sepeda))
            
            query = """
                    SELECT SP.nomor, SP.merk, SP.jenis, ST.nama as nama_stasiun,
                           SP.status, P.nama AS penyumbang
                    FROM SEPEDA SP, STASIUN ST, ANGGOTA A, PERSON P
                    WHERE SP.id_stasiun = ST.id_stasiun
                          AND SP.no_kartu_penyumbang = A.no_kartu
                          AND A.ktp = P.ktp;
                    """
            cursor.execute(query)
            data = dictfetchall(cursor)
            return render(request, 'daftar_sepeda.html', {'data_sepeda':data})

    else:
        with connection.cursor() as cursor:
            query = """
                    SELECT SP.nomor, SP.merk, SP.jenis, ST.nama as nama_stasiun,
                           SP.status, P.nama AS penyumbang
                    FROM SEPEDA SP, STASIUN ST, ANGGOTA A, PERSON P
                    WHERE SP.id_stasiun = ST.id_stasiun
                          AND SP.no_kartu_penyumbang = A.no_kartu
                          AND A.ktp = P.ktp;
                    """
            cursor.execute(query)
            data_sepeda = dictfetchall(cursor)
        return render(request, 'daftar_sepeda.html', {'data_sepeda':data_sepeda})

def create_sepeda(request):
    if (request.method == 'POST'):
        merk = request.POST['merk']
        jenis = request.POST['jenis']
        status = request.POST['status']
        stasiun = request.POST['stasiun']
        penyumbang = request.POST['penyumbang']

        # proses menyimpan ke sql (insert)
        with connection.cursor() as cursor:
            # untuk mencari nomor/id sepeda
            cursor.execute('SELECT MAX(nomor) FROM SEPEDA;')
            nomor_max = int(dictfetchall(cursor)[0]['max']) + 1
            
            query = '''
                    INSERT INTO SEPEDA VALUES
                    ('{}', '{}', '{}', '{}', '{}', {});
                    '''.format(nomor_max, merk, jenis, status, stasiun, penyumbang)
            cursor.execute(query)

        return redirect('daftar_sepeda')
    
    else: #jika baru masuk ke halaman ini
        with connection.cursor() as cursor:
            cursor.execute('SELECT * FROM STASIUN')
            stasiun_list = dictfetchall(cursor)

            cursor.execute('''SELECT A.*, P.*
                              FROM ANGGOTA A, PERSON P 
                              WHERE A.ktp = P.ktp;
                           ''')
            penyumbang_list = dictfetchall(cursor)

            response = {'stasiun_list': stasiun_list,
                        'penyumbang_list': penyumbang_list,
                        }
        return render(request, 'create_sepeda.html', response)

def update_sepeda(request):
    return render(request, 'update_sepeda.html')
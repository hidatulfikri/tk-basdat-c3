from django.urls import include, path
from .views import daftar_sepeda, create_sepeda, update_sepeda

urlpatterns = [
    path('', daftar_sepeda, name='daftar_sepeda'),
    path('create/', create_sepeda, name='create_sepeda'),
    path('update/', update_sepeda, name='update_sepeda'),
]

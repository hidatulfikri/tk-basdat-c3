from django.urls import include, path
from .views import daftar_voucher, create_voucher, update_voucher

urlpatterns = [
    path('', daftar_voucher, name='daftar_voucher'),
    path('create/', create_voucher, name='create_voucher'),
    path('update/', update_voucher, name='update_voucher'),
]

from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect

def daftar_voucher(request):
    cursor = connection.cursor()
    if request.method == 'POST':
            if request.POST.get('update-voucher'):
                id_voucher = request.POST['update-voucher']
                request.session['id_voucher'] = id_voucher
                return HttpResponseRedirect('update/')
            elif request.POST.get('delete-voucher'):
                 id_voucher = request.POST['delete-voucher']
                 cursor.execute("DELETE FROM VOUCHER WHERE id_voucher = %s", (id_voucher))
                 cursor.execute('''SELECT V.*, P.nama AS pengklaim
                                FROM VOUCHER V LEFT OUTER JOIN ANGGOTA A
                                ON V.no_kartu_anggota = A.no_kartu LEFT OUTER JOIN
                                PERSON P ON A.ktp = P.ktp;''')
                 data_acara = dictfetchall(cursor)
                 return render(request, 'daftar_voucher.html', {'data_voucher':data_voucher})
    else:
        cursor.execute('''SELECT V.*, P.nama AS pengklaim
                        FROM VOUCHER V LEFT OUTER JOIN ANGGOTA A
                        ON V.no_kartu_anggota = A.no_kartu LEFT OUTER JOIN
                        PERSON P ON A.ktp = P.ktp;''')
        data_voucher = dictfetchall(cursor)
        return render(request, 'daftar_voucher.html', {'data_voucher': data_voucher})

def create_voucher(request):
    if (request.method == 'POST'):
        nama = request.POST['nama']
        kategori = request.POST['kategori']
        nilai_poin = request.POST['nilai-poin']
        deskripsi = request.POST['deskripsi']
        jumlah = request.POST['jumlah']
        jumlah_int = int(jumlah)

        #proses insert data
        if(jumlah_int > 1):
             for i in jumlah :
                with connection.cursor() as cursor:
                     #untuk mencari id voucher
                     cursor.execute('SELECT COUNT(*) FROM VOUCHER')
                     row_count = cursor.fetchall()[0][0]
                     id_voucher = row_count + 1
                     
                     query = "INSERT INTO VOUCHER (id_voucher, nama, kategori, nilai_poin, deskripsi) VALUES (%s, %s, %s, %s, %s)"
                     cursor.execute(query, (id_voucher, nama, kategori, nilai_poin, deskripsi))
                return HttpResponseRedirect('/voucher/')
        else:
             with connection.cursor() as cursor:
                #untuk mencari id voucher
                cursor.execute('SELECT COUNT(*) FROM VOUCHER')
                row_count = cursor.fetchall()[0][0]
                id_voucher = row_count + 1
                
                query = "INSERT INTO VOUCHER (id_voucher, nama, kategori, nilai_poin, deskripsi) VALUES (%s, %s, %s, %s, %s)"
                cursor.execute(query, (id_voucher, nama, kategori, nilai_poin, deskripsi))
             return HttpResponseRedirect('/voucher/')
    else: #jika baru masuk ke halaman ini
        return render(request, 'create_voucher.html')

def update_voucher(request):
    id_voucher = request.session['id_voucher']
    cursor = connection.cursor()
    cursor.execute("SELECT no_kartu_anggota FROM VOUCHER WHERE id_voucher = %s", (id_voucher))
    data_voucher = dictfetchall(cursor)
    no_kartu_anggota = data_voucher['no_kartu_anggota']
    no_kartu_int = int(no_kartu_anggota)
    if request.method == 'POST':
            if (no_kartu_int == NULL):
                nama = request.POST['nama']
                kategori = request.POST['kategori']
                nilai_poin = request.POST['nilai-poin']
                deskripsi = request.POST['deskripsi']
                jumlah = request.POST['jumlah']
                jumlah_int = int(jumlah)
                print(request.POST)
                print(jumlah_int)

                if (jumlah_int > 1):
                   for i in jumlah_int:
                        query = '''UPDATE VOUCHER (nama, kategori, nilai_poin, deskripsi, jumlah) VALUES (%s, %s, %s, %s, %s)
                        WHERE id_voucher = %s '''
                        cursor.execute(query, (nama, kategori, nilai_poin, deskripsi, jumlah, id_voucher))
                   return HttpResponseRedirect('/voucher/')
                else:
                     query = '''UPDATE VOUCHER (nama, kategori, nilai_poin, deskripsi, jumlah) VALUES (%s, %s, %s, %s, %s)
                             WHERE id_voucher = %s '''
                     cursor.execute(query, (nama, kategori, nilai_poin, deskripsi, jumlah, id_voucher))
                     return HttpResponseRedirect('/voucher/')
            else:
                return HttpResponseRedirect('/voucher/')
    else:
            cursor.execute("SELECT * FROM VOUCHER WHERE id_voucher = %s", (id_voucher))
            data_voucher = dictfetchall(cursor)[0]
            cursor.execute('''SELECT V.*, P.nama AS pengklaim
                FROM VOUCHER V LEFT OUTER JOIN ANGGOTA A
                ON V.no_kartu_anggota = A.no_kartu LEFT OUTER JOIN
                PERSON P ON A.ktp = P.ktp;
                WHERE id_vocuher = %s''', (id_voucher))
            return render(request, 'update_voucher.html', {'data_voucher': data_vocuher})

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
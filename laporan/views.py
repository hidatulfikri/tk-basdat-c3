from django.shortcuts import render
from django.db import connection

def daftar_laporan(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT L.id_laporan, L.datetime_pinjam, P.nama, PJ.denda, L.status "
                       "FROM LAPORAN AS L, PEMINJAMAN AS PJ, PERSON AS P, ANGGOTA AS A "
                       "WHERE L.no_kartu_anggota = PJ.no_kartu_anggota "
                       "AND PJ.no_kartu_anggota = A.no_kartu "
                       "AND A.ktp = P.ktp")
        data_laporan = dictfetchall(cursor)
    return render(request, 'daftar_laporan.html', {'data_laporan':data_laporan})

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
from django.urls import include, path
from .views import daftar_stasiun, create_stasiun, update_stasiun

urlpatterns = [
    path('', daftar_stasiun, name='daftar_stasiun'),
    path('create/', create_stasiun, name='create_stasiun'),
    path('update/', update_stasiun, name='update_stasiun'),
]

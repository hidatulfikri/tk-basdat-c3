from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect

def daftar_stasiun(request):
    cursor = connection.cursor()
    if request.method == 'POST':
        if request.POST.get('update-stasiun'):
            id_stasiun = request.POST['update-stasiun']
            request.session['id_stasiun'] = id_stasiun
            return HttpResponseRedirect('update/')
        elif request.POST.get('delete-stasiun'):
            id_stasiun = request.POST['delete-stasiun']
            cursor.execute("DELETE FROM STASIUN WHERE id_stasiun = %s", (id_stasiun,))
            cursor.execute("SELECT * FROM STASIUN")
            data_stasiun = dictfetchall(cursor)
            return render(request, 'daftar_stasiun.html', {'data_stasiun':data_stasiun})
    else:
        cursor.execute("SELECT * FROM STASIUN")
        data_stasiun = dictfetchall(cursor)
        return render(request, 'daftar_stasiun.html', {'data_stasiun':data_stasiun})

def create_stasiun(request):
    cursor = connection.cursor()
    if request.method == 'POST':
        cursor.execute("SELECT COUNT(*) FROM STASIUN")
        row_count = cursor.fetchall()[0][0]
        id_stasiun = row_count + 1
        nama = request.POST['nama-stasiun']
        alamat = request.POST['alamat-stasiun']
        latitude = request.POST['latitude-stasiun']
        longitude = request.POST['longitude-stasiun']
        query = "INSERT INTO STASIUN VALUES (%s, %s, %s, %s, %s)"
        cursor.execute(query, (id_stasiun, alamat, latitude, longitude, nama))
        return HttpResponseRedirect('/stasiun/')
    else:
        return render(request, 'create_stasiun.html')

def update_stasiun(request):
    id_stasiun = request.session['id_stasiun']
    cursor = connection.cursor()
    if request.method == 'POST':
        nama = request.POST['nama-stasiun']
        alamat = request.POST['alamat-stasiun']
        latitude = request.POST['latitude-stasiun']
        longitude = request.POST['longitude-stasiun']
        query = "UPDATE STASIUN SET " \
                "nama = %s, " \
                "alamat = %s, " \
                "lat = %s, " \
                "long = %s " \
                "WHERE id_stasiun = %s"
        cursor.execute(query, (nama, alamat, latitude, longitude, id_stasiun))
        return HttpResponseRedirect('/stasiun/')
    else:
        cursor.execute("SELECT * FROM STASIUN WHERE id_stasiun = %s", (id_stasiun,))
        data_stasiun = dictfetchall(cursor)[0]
        return render(request, 'update_stasiun.html', {'data_stasiun':data_stasiun})

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
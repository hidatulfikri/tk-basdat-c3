# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'
# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Acara(models.Model):
    id_acara = models.CharField(primary_key=True, max_length=10)
    judul = models.CharField(max_length=100)
    deskripsi = models.TextField(blank=True, null=True)
    tgl_mulai = models.DateField()
    tgl_akhir = models.DateField()
    is_free = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'acara'


class AcaraStasiun(models.Model):
    id_stasiun = models.ForeignKey('Stasiun', models.DO_NOTHING, db_column='id_stasiun', primary_key=True)
    id_acara = models.ForeignKey(Acara, models.DO_NOTHING, db_column='id_acara')

    class Meta:
        managed = False
        db_table = 'acara_stasiun'
        unique_together = (('id_stasiun', 'id_acara'),)


class Anggota(models.Model):
    no_kartu = models.CharField(primary_key=True, max_length=10)
    saldo = models.FloatField(blank=True, null=True)
    points = models.IntegerField(blank=True, null=True)
    ktp = models.ForeignKey('Person', models.DO_NOTHING, db_column='ktp')

    class Meta:
        managed = False
        db_table = 'anggota'


class Laporan(models.Model):
    id_laporan = models.CharField(primary_key=True, max_length=10)
    no_kartu_anggota = models.ForeignKey('Peminjaman', models.DO_NOTHING, db_column='no_kartu_anggota')
    datetime_pinjam = models.DateTimeField()
    nomor_sepeda = models.CharField(max_length=10)
    id_stasiun = models.CharField(max_length=10)
    status = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'laporan'
        unique_together = (('id_laporan', 'no_kartu_anggota', 'datetime_pinjam', 'nomor_sepeda', 'id_stasiun'),)

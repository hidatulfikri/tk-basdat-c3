from django.urls import include, path
from .views import index, registrasi, registrasiPetugas, registrasiAnggota, login

urlpatterns = [
    path('', index, name='index'),
    path('accounts/', include('django.contrib.auth.urls')),
    path('accounts/signup/', registrasi, name='signup'),
    path('accounts/signup/petugas/', registrasiPetugas, name='registrasi_petugas'),
    path('accounts/signup/anggota/', registrasiAnggota, name='registrasi_anggota'),
    path('accounts/login', login, name='login'),
]

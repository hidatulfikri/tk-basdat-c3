from django.shortcuts import render
from .forms import *
from django.http import HttpResponseRedirect
from django.urls import reverse

def index(request):
    return render(request, 'index.html')

def registrasi(request):
    return render(request, 'registrasi.html')

def registrasiPetugas(request):
    if request.method == 'POST':
        user_form = UserForm(request.POST)
        person_form = PersonForm(request.POST)
        petugas_form = PetugasForm(request.POST)
        if user_form.is_valid() or person_form.is_valid() or petugas_form.is_valid():
            return HttpResponseRedirect(reverse('form-redirect') )
    else:
        user_form = UserForm()
        person_form = PersonForm()
        petugas_form = PetugasForm()

    return render(request, 'registrasi_petugas.html', {
        'user_form': user_form,
        'person_form': person_form,
        'petugas_form': petugas_form,
    })

def registrasiAnggota(request):
    if request.method == 'POST':
        user_form = UserForm(request.POST)
        person_form = PersonForm(request.POST)
        anggota_form = AnggotaForm(request.POST)
        if user_form.is_valid() or person_form.is_valid() or anggota_form.is_valid():
            return HttpResponseRedirect(reverse('form-redirect') )
    else:
        user_form = UserForm()
        person_form = PersonForm()
        anggota_form = AnggotaForm()

    return render(request, 'registrasi_petugas.html', {
        'user_form': user_form,
        'person_form': person_form,
        'anggota_form': anggota_form,
    })

def login(request):
    return render(request, 'login.html')
from django.apps import AppConfig


class OtorisasiConfig(AppConfig):
    name = 'otorisasi'

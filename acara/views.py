from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect

def daftar_acara(request):
    cursor = connection.cursor()
    if request.method == 'POST':
        if request.POST.get('update-acara'):
            id_acara = request.POST['update-acara']
            request.session['id_acara'] = id_acara
            return HttpResponseRedirect('update/')
        elif request.POST.get('delete-acara'):
            id_acara = request.POST['delete-acara']
            cursor.execute("DELETE FROM ACARA WHERE id_acara = %s", (id_acara,))
            cursor.execute("SELECT * FROM ACARA")
            data_acara = dictfetchall(cursor)
            return render(request, 'daftar_acara.html', {'data_acara':data_acara})
    else:
        cursor.execute("SELECT * FROM ACARA")
        data_acara = dictfetchall(cursor)
        return render(request, 'daftar_acara.html', {'data_acara':data_acara})

def create_acara(request):
    cursor = connection.cursor()
    if request.method == 'POST':
        cursor.execute("SELECT COUNT(*) FROM ACARA")
        row_count = cursor.fetchall()[0][0]
        id_acara = row_count + 1
        judul = request.POST['judul-acara']
        deskripsi = request.POST['deskripsi-acara']
        if request.POST['is-gratis'] == 'Ya':
            is_gratis = True
        else:
            is_gratis = False
        tanggal_mulai = request.POST['tanggal-mulai']
        tanggal_selesai = request.POST['tanggal-selesai']
        stasiun = request.POST.getlist('stasiun')
        query = "INSERT INTO ACARA VALUES (%s, %s, %s, %s, %s, %s)"
        cursor.execute(query, (id_acara, judul, deskripsi, tanggal_mulai, tanggal_selesai, is_gratis))
        for s in stasiun:
            cursor.execute("SELECT id_stasiun FROM STASIUN WHERE nama = %s", (s,))
            id_stasiun = cursor.fetchall()[0]
            query = "INSERT INTO ACARA_STASIUN VALUES (%s, %s)"
            cursor.execute(query, (id_stasiun, id_acara))
        return HttpResponseRedirect('/acara/')
    else:
        cursor.execute("SELECT * FROM STASIUN")
        data_stasiun = dictfetchall(cursor)
        return render(request, 'create_acara.html', {'data_stasiun':data_stasiun})

def update_acara(request):
    id_acara = request.session['id_acara']
    cursor = connection.cursor()
    if request.method == 'POST':
        judul = request.POST['judul-acara']
        deskripsi = request.POST['deskripsi-acara']
        if request.POST['is-gratis'] == 'Ya':
            is_gratis = True
        else:
            is_gratis = False
        tanggal_mulai = request.POST['tanggal-mulai']
        tanggal_selesai = request.POST['tanggal-selesai']
        stasiun = request.POST.getlist('stasiun')
        query = "UPDATE ACARA SET " \
                "judul = %s, " \
                "deskripsi = %s, " \
                "tgl_mulai = %s, " \
                "tgl_akhir = %s, " \
                "is_free = %s " \
                "WHERE id_acara = %s"
        cursor.execute(query, (judul, deskripsi, tanggal_mulai, tanggal_selesai, is_gratis, id_acara))
        cursor.execute("DELETE FROM ACARA_STASIUN WHERE id_acara= %s", (id_acara,))
        for s in stasiun:
            cursor.execute("SELECT id_stasiun FROM STASIUN WHERE nama = %s", (s,))
            id_stasiun = cursor.fetchall()[0]
            query = "INSERT INTO ACARA_STASIUN VALUES (%s, %s)"
            cursor.execute(query, (id_stasiun, id_acara))
        return HttpResponseRedirect('/acara/')
    else:
        cursor.execute("SELECT * FROM ACARA WHERE id_acara = %s", (id_acara,))
        data_acara = dictfetchall(cursor)[0]
        cursor.execute("SELECT S.nama FROM STASIUN AS S, ACARA_STASIUN AS A "
                       "WHERE A.id_acara = %s "
                       "AND S.id_stasiun = A.id_stasiun ", (id_acara,))
        stasiun_dipilih = dictfetchall(cursor)
        cursor.execute("SELECT * FROM STASIUN")
        data_stasiun = dictfetchall(cursor)
        return render(request, 'update_acara.html', {'data_stasiun':data_stasiun, 'data_acara':data_acara, 'stasiun_dipilih':stasiun_dipilih})

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

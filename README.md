# TK Basdat C3

[![build status](https://gitlab.com/hidatulfikri/tk-basdat-c3/badges/master/build.svg)](https://gitlab.com/hidatulfikri/tk-basdat-c3/commits/master)
[![coverage status](https://gitlab.com/hidatulfikri/tk-basdat-c3/badges/master/coverage.svg)](https://gitlab.com/hidatulfikri/tk-basdat-c3/commits/master)

Basis Data C - TK5 - Kelompok 3 [Heroku Link](https://tk-basdat-c3.herokuapp.com/)

Anggota:

- Hidayatul Fikri (1706022880)
- Laksono Bramantio (1706984650)
- Rizki Leonardo (1606878934)
- Yasmin Amalia (1706043720)


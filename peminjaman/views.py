from django.shortcuts import render
from django.db import connection

def daftar_peminjaman(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT A.no_kartu, S.nomor, S.merk, ST.id_stasiun, ST.nama, PJ.datetime_kembali, PJ.biaya, PJ.denda "
                       "FROM PEMINJAMAN AS PJ, ANGGOTA AS A, SEPEDA AS S, STASIUN AS ST "
                       "WHERE PJ.no_kartu_anggota = A.no_kartu "
                       "AND PJ.nomor_sepeda = S.nomor "
                       "AND PJ.id_stasiun = ST.id_stasiun")
        data_peminjaman = dictfetchall(cursor)
    return render(request, 'daftar_peminjaman.html', {'data_peminjaman':data_peminjaman})

def create_peminjaman(request):
    return render(request, 'create_peminjaman.html')

def update_peminjaman(request):
    return render(request, 'update_peminjaman.html')

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
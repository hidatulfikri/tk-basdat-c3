from django.urls import include, path
from .views import daftar_peminjaman, create_peminjaman, update_peminjaman

urlpatterns = [
    path('', daftar_peminjaman, name='daftar_peminjaman'),
    path('create/', create_peminjaman, name='create_peminjaman'),
    path('update/', update_peminjaman, name='update_Peminjaman'),
]
